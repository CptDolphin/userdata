package pl.sda.userdata.user;

import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.userdata.database.HibernateUtil;
import pl.sda.userdata.database.JPAUtil;

import java.util.*;

public class UserDAO {
    //data acces object
    private SessionFactory sessionFactory;

    private static final Map<Integer, User> users = new HashMap<>();

    public UserDAO() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    public Optional<User> getUserById(int id) {
        Session session = sessionFactory.openSession();
        User value = session.find(User.class, id);
        session.close();
        return Optional.ofNullable(value);
    }

    public List<User> getAll() {
        Session session = sessionFactory.openSession();
        List<User> userList = session.createQuery("from User").getResultList();
        session.close();
        return userList;
    }

    public boolean addUser(User user) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(user);
        transaction.commit();
        session.close();
        return true;
    }

    public boolean removeUser(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            User user = session.load(User.class,id);
            session.contains(user);
            session.delete(user);
        }catch (ObjectNotFoundException onfe){
            onfe.printStackTrace();
            return false;
        }
        transaction.commit();
        session.close();
        return true;
    }

    public int editUser(Integer userId, String name) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            User user = session.load(User.class, userId);
            session.contains(user);
            user.setName(name);
        } catch (ObjectNotFoundException onfe) {
            onfe.printStackTrace();
        }
        transaction.commit();
        session.close();
        return userId;
    }
}
